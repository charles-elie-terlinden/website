# Planteur de patates

## Tutoriel (FR)

Les étapes :
1) Conception du châssis 
2) Fabrication des outils et du réservoir
3) Système de dépose des patates
4) La mise sur roue
5) Assemblage

![image test](image/proto.jpg)

_____________________

1) Conception du châssis

Matériaux : Les deux vélos
Outils : Scie à métaux, clés à laine, clés à molette, tourne vis

![image test](image/cadre_fixie_gris_billant.jpg)

A - Démonter le vélo
Démontez tous les éléments inutiles tels que ; le pédalier, les vitesses, les câbles, la selle...
Retirez les roues et garder les de coté elles serviront pour la suite

B - Démonter le deuxième vélo ou récupérer les pièces nécessaire.
Découpez le vélo de façon à récupérer la fourche avant ainsi que le guidon puis le triangle arrière du châssis

![image test](image/20200107_153332.jpg)

![image test](image/20200107_153351.jpg)


2) Fabrication des outils et du réservoir

Matériaux : Baril d'huile de 200L, fourche, pédalier
Outils : Scie à métaux, un marqueur, plusieurs feuilles de papier

            A - Découpe du réservoir
Découpez en deux le baril longitudinalement de façon à ce qu'il y ait deux morceaux de dimension identique :

![image test](image/20200107_105935.jpg)

Prenez la partie sur laquelle ne figure pas le trou du bouchon
Sur l'une des extrémités plate, percez une ouverture située au même endroit que sur la photo ci-dessous de 10cm, elle servira pour la sortie des patates.

![image test](images/test.jpg)(Photo du trou baril)

          B - Découpe des outils
Munissez-vous maintenant de l'autre moitié, celle-ci va servir pour fabriquer les outils.
A l'aide de plusieurs feuilles et d 'un feutre, dessinez le patron du première outil. Cet outil se trouvera à l'avant de la machine, il aura comme effet de creuser le sillon.
Découpez le patron puis reportez le deux fois de façon symétrique sur le baril, voir image ci-dessous

![image test](image/20200107_105049.jpg)(Photo du patron et du baril 1er outil)

Procédez de la même façon pour la fabrication du deuxième outil sauf que celui-ci aura une forme arrondie plate, il servira pour refermer le sillon.
Pour ce faire, à l'aide du deuxième patron ( voir photo ) que vous aurez fait, reportez-le sur les deux extrémités plane du baril

![image test](image/20200107_111211.jpg)

Pour finir découpez les barils en suivant les lignes.

![image test](image/20200107_153315.jpg )

          C – Assemblage du premier outil 
Pour ce faire, deux façon sont possible : soit l'assemblage par soudure soit par boulonnage.
Soudez ou assemblez les deux morceaux identiques de façon à ce que les deux arrêtes coupées en biais  forment une pointe, voir la photo ci-dessous

          D – Assemblage du deuxième outil
Vissez les deux parties d'outil de forme ronde, respectivement sur le morceau de pédalier de façon à ce que la vis soit placée au centre du cercle.

Ceci fait, fixez maintenant l'autre extrémité du pédalier sur la partie haute de la fourche sur une extrémité, de façon à ce que l'outil soit un peut de biais. 
Faites de même pour la deuxième. Fixez le tout sur l'autre tube de la fourche.

![image test](image/20200108_182631.jpg)

3) Système de dépose des patates

Matériaux : grille de ventilateur, bouteille en plastique, boite de conserve (x3), morceau de baril
Outil : scie a métaux, 

A – Emplacement des boites de conserve
A l'aide d'un feutre, tracez sur la grille du ventilateur trois parties égales comme le signe peace and love, de façon à ce que la grille soit divisée en trois parties égales. Ce tracer va nous servir d'axe de percement.


Découpez en rond  la grille pour pouvoir placer les trois boites à l'intérieur de la grille sachant qu'un axe correspond à chaque fois au centre d'une boite.

![image test](image/20200109_111323.jpg)


B - Renfort axe de rotation 
Découpez dans une partie plane du baril deux carrés identiques de dimension 70mm sur 70 mm.
Vissez les carrés à chaque extrémité axiale de la grille 	
Percez les carrés en leur centre, de diamètre correspondant à votre axe.


4) La mise sur roue
Matériaux : gente de mobylette, roue de vélo, axe, 
Outil : Clés à molette, vise, écrou


5) Assemblage

A - Fixation outil arrière
Système de réglage hauteur vis écrou

B - Fixation outil avant 
Système de levage câble de frein

C - Fixation réservoir

D - Liaison réservoir/dépose patate

________________________

# Potato machine

## Tutorial (EN)

Steps :
1) Chassis design
2) Tool and tank manufacturing
3) Potato removal system
4) Putting on wheel
5) Assembly

![image test](image/proto.jpg)

_________________________

1) Chassis design

Materials: The two bikes
Tools: Hacksaw, woolen wrenches, adjustable wrenches, screwdriver

![test image](image/cadre_fixie_gris_billant.jpg)

A - Disassemble the bike
Disassemble all unnecessary items such as; the pedals, the gears, the cables, the saddle ...
Remove the wheels and keep them aside they will be used later

B - Disassemble the second bike or collect the necessary parts.
Cut the bike so as to recover the front fork as well as the handlebars then the rear triangle of the chassis

![test image](image/20200107_153332.jpg)

![test image](image/20200107_153351.jpg)


2) Tool and tank manufacturing

Materials: 200L oil barrel, fork, crankset
Tools: Hacksaw, marker, multiple sheets of paper

            A - Tank cutting
Cut the barrel in half lengthwise so that there are two pieces of identical size:

![test image](image/20200107_105935.jpg)

Take the part on which the hole of the cap is not shown
On one of the flat ends, drill an opening located in the same place as in the photo below of 10cm, it will be used for the exit of the potatoes.

![image test](images/test.jpg) (Photo of the barrel hole)

          B - Cutting tools
Now bring the other half, which will be used to make the tools.
Using several sheets and a marker, draw the pattern for the first tool. This tool will be located at the front of the machine, it will have the effect of digging the groove.
Cut out the pattern then transfer it symmetrically twice to the barrel, see image below

![image test](image/20200107_105049.jpg) (Photo of the pattern and barrel 1st tool)

Proceed in the same way for the manufacture of the second tool except that it will have a flat rounded shape, it will be used to close the groove.
To do this, using the second pattern (see photo) that you have made, transfer it to the two flat ends of the barrel

![test image](image/20200107_111211.jpg)

Finally cut out the barrels following the lines.

![test image](image/20200107_153315.jpg)

          C - Assembly of the first tool
There are two ways to do this: either assembly by welding or bolting.
Solder or join the two identical pieces so that the two edges cut at an angle form a point, see the photo below

          D - Assembly of the second tool
Screw the two round tool parts, respectively, onto the bottom bracket so that the screw is placed in the center of the circle.

This done, now fix the other end of the crankset on the upper part of the fork on one end, so that the tool is a little bias.
Do the same for the second. Attach everything to the other fork tube.

![test image](image/20200108_182631.jpg)

3) Potato removal system

Materials: fan grille, plastic bottle, tin can (x3), barrel piece
Tool: hacksaw,

A - Location of cans
Using a felt pen, trace on the fan grille three equal parts like the peace and love sign, so that the grid is divided into three equal parts. This plot will serve us as a piercing axis.


Cut out the grid in the round to be able to place the three boxes inside the grid, knowing that an axis corresponds each time to the center of a box.

![test image](image/20200109_111323.jpg)


B - Axis of rotation reinforcement
Cut in a flat part of the barrel two identical squares of dimension 70mm by 70 mm.
Screw the squares to each axial end of the grid
Drill the squares in their center, with a diameter corresponding to your axis.


4) Putting on wheel
Materials: moped, bike wheel, axle,
Tool: Adjustable wrenches, sight, nut


5) Assembly

A - Rear tool attachment
Screw nut height adjustment system

B - Front tool attachment
Brake cable lifting system

C - Tank fixing

D - Tank connection / potato removal