# ULB - Fablab Studio

> **Comment la fabrication numérique et le réseau des fablabs peuvent-ils aider à transformer la manière dont Cuba et ses habitants entrent en résilience?**

![Adapto](images/cuba.jpg)

Il existe une période particulière à Cuba qui a placé l'île dans une situation de crise importante. Cette période appelée "période spéciale" a grandement marqué les habitants de l'île et les a forcés, ainsi que le gouvernement, à trouver des solutions pour survivre dans une situation de pénurie généralisée.

Cette période longue de 16 ans a commencé à la sortie de la domination russe en 1993. Fidel Castro et son gouvernement ont décidé à ce moment de collecter les solutions disséminées dans toute l'île pour résoudre les problèmes quotidiens des cubains. Un livre en est sorti : "Con nos proprios effuertos" (Par nos propres efforts), livre qui passe au travers de tous les domaines de la vie cubaine, montrant comment réparer voitures, vélos, machines, comment cultiver, comment construire … avec uniquement les moyens présents sur l'île.

Ce livre est une des explications à la formidable capacité des cubains à fabriquer et réparer à peu près n'importe quoi. La situation de Cuba est en cours d'évolution et le pays est entré dans une période post-castriste qui s’accompagne aussi pour la première fois, d’un accès à l'information presque équivalent au nôtre au travers du net. Un référendum à également transformé le rapport des habitants de l'île avec le pouvoir centralisateur.

Il n'existe pas de fablab à Cuba mais nous avons établi ces derniers mois des connexions avec des groupes actifs de makers qui travaillent avec des outils numériques à peu près similaires à ceux présents dans nos fablabs. Ils sont au début du chemin menant vers ces nouvelles manières de produire et ont montré d’emblée un enthousiasme, un sens de l’observation et des connaissances étonnants.

Nous proposons cette année de travailler sur un projet entre fiction et réalité avec l'idée d'un impact possible sur l'île. Nous prendrons comme base le livre "Con nos proprios effuertos".

Les étudiants choisiront un thème, un objet, un processus qu’ils croiseront avec les outils numériques pour produire des projets en open-source et reproductibles qu'ils partageront sur le net. Les étudiants documenteront leurs projets pour qu'ils puissent être lus, reproduits, améliorés, transformés par des habitants de l'île.

Un dialogue est déjà établi avec des architectes, designers, makers et geeks à Cuba qui seront nos experts de terrain et pourront nous aiguiller pour la définition des projets.
Une résidence d'une semaine au FabLab ULB fera partie du parcours de cette année avec le designer cubain Ernesto Oroza dont le livre « Objets réinventés » sert régulièrement de référence sur la manière dont les Cubains travaillent la conception de l'objet.

Le semestre se déroulera de la manière suivante:

#### Semaines 1 à 4
* Apprentissage des outils numériques au FabLab ULB.
* Lecture et analyse du livre édité lors de la période spéciale.
* Présentation et analyse de l'histoire de Cuba.

#### Semaine 5
* Workshop, séminaire avec Ernesto Oroza
#### Semaines 6 à 12
* Définition du projet.
* Prototypage.
* Documentation du process. Tests …
* Remise finale/ Jury
* Un projet reproduisible, documenté, fonctionnel et une documentation de la démarche et du process de conception.
