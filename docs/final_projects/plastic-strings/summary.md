# Plastic string

## Summary

![image test](images/plastic.jpg)

### Problematic (EN):
The PET plastic bottle invades Cuba with the economy based almost exclusively on tourism, the UERMP (The Union of Enterprises for the Recovery of Raw Materials) is no longer enough to stem their excessive presence on the island.

### Question:

> How to create an alternative to recycling PET plastic bottles that can give them added value and easily usable by Cubans?

_Hypothesis :_
<ul> <li> The first step is to create added value for the plastic bottle, an alternative to direct resale.
We recover and conserve more thanks to the action of the inhabitants.

<li> The second step is the transformation of the bottle into a material, more precisely into threads.
They are tapered thanks to a cutter designed for the plastic bottle, a simple tool to produce, inexpensive in materials and easily spread on the island.
What to do with the wire, what advantage?

<li> Third step, crafts!
The creation of a catalog of objects, feather duster brooms, geotextile fabrics, bags, ... for personal use or to resell to tourists.

______________________

## Résumé

### Problématique (FR) : 
La bouteille plastique PET envahit Cuba avec l'économie basée presque exclusivement sur le tourisme, l'UERMP (The Union of Enterprises for the Recovery of Raw Materials) ne suffit plus à endiguer leur présence excessive sur l'île.

### Question :
> Comment créer une alternative au recyclage des bouteilles plastiques PET pouvant leur conférer une plus-value et facilement utilisable par les cubains?

_Hypothèse :_
<ul><li>La première étape est de créer une plus-value à la bouteille plastique, une alternative à la revente directe.
On en récupère et conserve davantage grâce à l'action des habitants. 

<li>La seconde étape est la transformation de la bouteille, en matériau, plus précisément en fils. 
On les effile grâce à un cutter designé pour la bouteille plastique, un outil simple à produire, peu couteux en matériaux et facilement répandable sur l'île.
Que faire du fil, quel avantage?

<li>Troisième étape, l'artisanat!
La création d'un catalogue d'objets, plumeau de balais, toiles géotextiles, sacs, ... à usage personnel ou à revendre aux touristes.

______________________

## Resumen : 

### Problemática (ES):
La botella de plástico PET invade Cuba con una economía basada casi exclusivamente en el turismo, la UERMP (Unión de Empresas para la Recuperación de Materias Primas) ya no es suficiente para detener su presencia excesiva en la isla.

### Pregunta :
> ¿Cómo crear una alternativa al reciclaje de botellas de plástico PET que puedan darles un valor agregado y que los cubanos puedan usarlas fácilmente?

_Hipótesis:_

<ul> <li> El primer paso es crear valor agregado para la botella de plástico, una alternativa a la reventa directa.
Recuperamos y conservamos más gracias a la acción de los habitantes.

<li> El segundo paso es la transformación de la botella en un material, más precisamente en hilos.
Se ahusan gracias a un cortador diseñado para la botella de plástico, una herramienta simple para producir, económica en materiales y fácil de extender en la isla.
¿Qué hacer con el cable, qué ventaja?

<li> Tercer paso, manualidades!
La creación de un catálogo de objetos, escobas de plumero, telas geotextiles, bolsos, ... para uso personal o para revender a los turistas.
