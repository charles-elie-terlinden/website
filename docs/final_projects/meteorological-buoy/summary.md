# Meteorological Buoy

## Summary

![Meteorological Buoy](images/Slide-11.jpg)

### Problem

The weather, in the form of storms especially, has the power to cause a lot of destruction. But if it can be predicted, a lot of problems can be easily avoided. This information can be taken advantage of in many other ways too. The problem is just that Cuba doesn’t have the adequate means to take measurements. They mainly have weather stations on the land, but because the oceans have a big influence on the weather too, these should also be studied.

### Question

Because Meteorological buoys that do this are generally very expensive, we want to develop a simple meteorological buoy that can be recreated a lot cheaper. We wonder what data are useful to collect ?

_Hypothesis_

Our objectives :

- The buoy has to measure wind speed and direction, air pressure and humidity, water temperature and the wave height, direction and period.
- The buoy has to be autonomous for at least a year.
- The buoy has to survive this long too.

How will we do this?

- Electronical sensors will be used to measure the properties out at sea.
- Two movable parts will collect data for the wind speed and direction.
- This data will be processed and transmitted to the shore by an arduino.
- The electronics will be powered by a battery in combination with solar panels.
- The buoy has to be waterproof to protect the electronics.
- The hull has to be strong and shock resistant.


## Résumé

### Problématique

La météo, sous forme de tempêtes en particulier, a un pouvoir destructif. Mais si elle peut être prédite, beaucoup de problèmes peuvent être facilement évités. Cette information peut être exploitée de bien d'autres façons également. Le problème, c'est que Cuba n'a pas les moyens adéquats pour prendre des mesures dans le domaine maritime. Ils ont principalement des stations météorologiques sur la terre ferme, mais comme les océans ont aussi une grande influence sur le temps qu'il fait, il faut aussi les étudier.

### Question

Les bouées météorologiques qui font cela sont généralement très chères, c’est pourquoi nous voulons développer une simple bouée météorologique qui peut être recréée beaucoup moins cher. Nous nous demandons quelles données sont utiles à collecter ?

_Hypothèses_

Nos objectifs :

- La bouée doit mesurer la vitesse et la direction du vent, la pression et l'humidité de l'air, la température de l'eau, la hauteur, la direction et la période des vagues.
- La bouée doit être autonome pendant au moins un an.
- La bouée doit survivre aussi longtemps.

Comment y parviendrons-nous ?

- Des capteurs électroniques seront utilisés pour mesurer les propriétés en mer.
- Deux parties mobiles recueilleront des données sur la vitesse et la direction du vent.
- Ces données seront traitées et transmises au rivage par un arduino.
- L'électronique sera alimentée par une batterie en combinaison avec des panneaux solaires.
- La bouée doit être étanche pour protéger l'électronique.
- La coque doit être solide et résistante aux chocs.

## Resumen

### Problemàtica

El clima, especialmente en forma de tormentas, tiene el poder de causar mucha destrucción. Pero si se puede predecir, muchos problemas se pueden evitar fácilmente. Esta información puede ser aprovechada de muchas otras maneras también. El problema es que Cuba no tiene los medios adecuados para tomar medidas. Principalmente tienen estaciones meteorológicas en tierra, pero debido a que los océanos también tienen una gran influencia en el clima, estos también deberían ser estudiados.

### Pregunta

Debido a que las boyas meteorológicas que hacen esto son generalmente muy costosas, queremos desarrollar una boya meteorológica simple que pueda ser recreada mucho más barata. Nos preguntamos qué datos son útiles para recopilar.

_Hipotesis_

Nuestros objetivos

- La boya tiene que medir la velocidad y dirección del viento, la presión y humedad del aire, la temperatura del agua y la altura de la ola, dirección y período.
- La boya debe ser autónoma durante al menos un año.
- La boya también tiene que sobrevivir este tiempo.

¿Cómo lo haremos?

- Se utilizarán sensores electrónicos para medir las propiedades en el mar.
- Dos partes móviles recogerán datos de la velocidad y dirección del viento.
- Estos datos serán procesados y transmitidos a la orilla por un arduino.
- La electrónica será alimentada por una batería en combinación con paneles solares.
- La boya debe ser impermeable para proteger la electrónica.
- El casco debe ser fuerte y resistente a los golpes.
