# Title of your project

## Tutorial

Voici le tutoriel pour fabriquer un cuiseur solaire permettant de pasteuriser l'eau et les conserves.

## Fabrication du cuiseur solaire

## Matériaux

<html>
<table>
<tr align=top>
	<td width="25%">
	<img src="docs/final_projects/four-solaire/images/Diapositive17.JPG">
     </td>
     <td>
    <p><b>Matériaux</b>
    <br> Pour la structure :
    <br> - Carton ou plaque de bois, grillage...
    <br>Pour les miroirs
<br> - Papier aluminium ou canettes découpés
<br>Autres :
<br>- Ficelles
<br>- Pinces à linge ou élastiques
<br>- Ruban adhesif marron
<br>- Colle 
<br>Remarque : la colle peut être faite à base de polystyrène dilué dans de l'acétone ou à base de farine et d'eau.

<b>Outils</b>
<br> - Cutter
<br>- Régle, équerre, rapporteur
<br>- Un crayon à papier
</td>

</tr>
</table>

## Fabrication

### Etape 1

<table>
<tr align=top>
	<td width="70%">
	<img src="docs/final_projects/four-solaire/images/PLANOK.jpg">
     </td>
     <td>
    Reporter les dimensions de chaque élément sur les cartons selon le plan indiqué (voir image).
    <br>Remarque : les pointillés vert correspondent aux plis, les points rouges sont des trous de 4 mm de diamètre.
     </td>
</tr>
</table>

### Etape 2

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2355.JPG">
     </td>
     <td>
    Découper les éléments à l'aide d'un cutter selon les tracés effectués précédement.
    <br> Autre procédé : La découpe peut s'effectuer grâce à une découpeuse laser. 
     </td>
</tr>
</table>

### Etape 3

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2177.JPG">
     </td>
     <td>
    Assembler les éléments avec du ruban adhésif marron comme sur la photo.
    <br>Remarque : appliquer le ruban adhésif sur les deux faces du cuiseur.
     </td>
</tr>
</table>

### Etape 4

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2182.JPG">
     </td>
     <td>
    Coller le papier aluminium sur la face avant du carton.
    <br>Autre procédé : découper des canettes de façon à avoir des plaque plates que l'on peut ensuite percées et accroché sur la structure à l'aide de fil de fer.
     </td>
</tr>
</table>

### Etape 5

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2390.JPG">
     </td>
     <td>
   Fixer les contrepoids à l'arrière du panneau arrière à l'aide de ruban adhésif et de ficelle comme sur la photo.
     </td>
</tr>
</table>

### Etape 6

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2185.JPG">
     </td>
     <td>
     Dans cette étape on va créer une ficelle pour orienter le panneau avant selon un angle particulier pour chaque saison.
   <br>A) Découper au moins 1m de ficelle
   <br>B) Faite un premier noeud à l'extrémité de la ficelle puis placer les autres noeuds selon les distances suivante (mesurées depuis le premier noeud) :
   <br> - 1 noeud à 80,4 cm pour l'été (Orientation du panneau avant = 45°)
   <br>- 1 noeud à 88,3 cm pour le printemps et l'automne (Orientation du panneau avant = 30°)
   <br>- 1 noeud à 96,8 cm pour l'hiver (Orientation du panneau avant = 10°)
   <br>C) Insérer des bout de fil de fer dans les noeuds 
   <br>D) Attacher la ficelle en l'insérant du coté du premier noeud dans le trou du panneau avant, faite un second noeud de manière à avoir un noeud de chaque coté du carton.
     </td>
</tr>
</table>

### Etape 7

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2377.JPG">
     </td>
     <td>
     Dans cette étape on va procéder au montage :
     <br>A) Rassembler les panneaux arrière et latéraux et les attacher avec des pinces à linges ou des élastiques
     <br>B) Assembler les contrepoids comme sur la photo puis placer un poids dessus (bocal rempli d'eau)
    <br>C) Accrocher la ficelle dans l'interstice en haut du panneau arrière en choisissant la bonne orientation indiqué par chaque noeuds (voir Etape 6)
     </td>
</tr>
</table>

## Fabrication de la marmite

## Matériaux

<html>
<table>
<tr align=top>
	<td width="25%">
	<img src="docs/final_projects/four-solaire/images/marmite.jpg">
     </td>
     <td>
    <p><b>Matériaux</b>
    <br> Pour le récipient :
    <br> - Grandes boites de conserves de 13cm de diamètre et 15 cm de haut (ici : boite de lait infantile)
    <br>- Peinture noire
    <br> Pour le couvercle :
    <br>- Grandes boites de conserves identiques au récipients.
    <br>- Fil de fer
    <br>- Tissu
    <br> Pour l'indicateur :
    <br> - 2 Canettes 
    <br> - 1 stylo bille transparent
    <br> - Mastic d'étanchéité
    <br> - Colorant (curry, mercurochrome...)
    <br> - Eau
<b>Outils</b>
<br> - Cutter
<br> - Ciseaux 
<br>- Scie à métaux
<br>- Marqueur
<br> - Régle (ruler)
</td>

</tr>
</table>

## Fabrication

### Etape 1

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2232.JPG">
     </td>
     <td>
    Peindre la boite de conserve en noir
     </td>
</tr>
</table>

### Etape 2

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2325.JPG">
     </td>
     <td>
    -Découper le bas d'une seconde boite de conserve, de même diamètre que la précédente, pour réaliser le couvercle.
   <br>- Percer deux trous diamétralement opposés.
     </td>
</tr>
</table>

### Etape 3

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2326.JPG">
     </td>
     <td>
    Accrocher du tissu sur la circonférence de manière à couvrir la partie tranchante et permettre une meilleure régularité.
     </td>
</tr>
</table>

### Etape 4

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2249.JPG">
     </td>
     <td>
   - Démonter entièrement le stylo bille (bouchon arrière compris) pour obtenir une sorte de paille.
   <br>- Relever le diamtère du stylo
     </td>
</tr>
</table>

### Etape 5

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2334.JPG">
     </td>
     <td>
   Percer un trou de 8mm (correspondant au diamètre du stylo) au milieu du couvercle
     </td>
</tr>
</table>

## Fabrication de l'indicateur de température

### Etape 6

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2299.JPG">
     <td>
   Tracer les marquages pour la découpe des canettes, on souahaite récupéer le fond des canettes :
   <br>- Faite un marquage à 1,5 cm et un autre à 1 cm du bord
     </td>
</tr>
</table>

### Etape 7

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2305.JPG">
     <td>
   <br>-Découper les canettes selon le marquage.
   <br>- Percer le morceau de canette de 1 cm selon le diamètre du stylo (8 mm)
     </td>
</tr>
</table>

### Etape 7

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/curry.jpg">
     <td>
   Verser une pincée de colorant (curry) dans la coupelle non trouée.
     </td>
</tr>
</table>

### Etape 8

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/eau.jpg">
     <td>
   <br>- Encastrer la coupelle trouée dans l'autre coupelle de façon à ce que leurs bord tranchés soit au même niveau.
   <br>- Verser de l'eau par le trou jusqu'au raz bord. Agiter le tout.
     </td>
</tr>
</table>

### Etape 9

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/styloK.jpg">
     <td>
   <br>- Enfoncer le stylo dans le trou
   <br>- Appliquer du mastic d'étanchéité à la base du stylo
     </td>
</tr>
</table>

### Etape 10

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2321.JPG">
     <td>
   <br> Tester l'indicateur en le plaçant dans une casserole d'eau bouillante. Quand l'eau boue indiquer le niveau de l'eau colorée dans le stylo en traçant une marque dessus.
     </td>
</tr>
</table>

### Etape 11

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2341.JPG">
     <td>
   <br> - Insérer l'indicateur dans le couvercle
     </td>
</tr>
</table>

### Etape 12

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2344.JPG">
     <td>
   <br> Percer un trou en haut de la boite de conserve et accrocher le couvercle avec du fil de fer.
     </td>
</tr>
</table>

### Etape 13

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2347.JPG">
     <td>
   <br>Accrocher une ficelle dans l'anneau à l'avant du couvercle qui retient le tissu
     </td>
</tr>
</table>

### Etape 14

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2352.JPG">
     <td>
   <br>Envelopper le bocal à pasteuriser avec du filet à oignons, patates ou agrumes pour pouvoir le retirer sans vous brûler à la fin de la pasteurisation à 100°C.
     </td>
</tr>
</table>

### Fabrication de la cloche protectrice

### Etape 1

<table>
<tr align=top>
	<td width="35%">
	<img src="docs/final_projects/four-solaire/images/IMG_2379.JPG">
     <td>
   <br>- Découper deux bouteille de 5L d'eau (PET) pour former une bouteille ouvrable et refermable par emboitement
   <br>- Placer un petit bocal en verre au fond de la bouteille.
     </td>
</tr>
</table>

## Assemblage

<table>
<tr align=top>
	<td width="25%">
	<img src="docs/final_projects/four-solaire/images/IMG_2381.JPG">
     </td>
     <td width="25%">
    <img src="docs/final_projects/four-solaire/images/IMG_2386.JPG">
    </p>
    </td>
</tr>
</table>
</html>
