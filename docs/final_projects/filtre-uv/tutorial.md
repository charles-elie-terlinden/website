<h1>Filtre UV-C</h1>

<h2>Tutorial</h2>

<h3>1. 	Explication du filtre :</h3>

Le filtre étant composé de plastique (ASA), les UV n'auront aucune conséquence sur la chambre à UV-C. Les bouteilles en plastique (PET), par leurs goulots, se fixent aisément au filtre. L’eau dans les 2 bouteilles supérieures coule à l’aide de la gravité dans la chambre à UV-C, où elle est traitée par les rayons. Par la suite elle s'écoule, toujours à l’aide de la gravité, dans les bouteilles (vides) inférieures. La procédure de filtration prend environ 90 secondes grâce aux orifices de différentes tailles et peut être répété à volonté. 

![image en ligne](images/impression-2.jpeg)

**Informations importantes à savoir :**

- Plus l’eau à filtrer est claire, translucide, au mieux les UV-C peuvent atteindre les bactéries et les tuer. 

- Le filtre est conçu pour laisser passer 1L d’eau en +-90 secondes. Il peut donc être connecté avec des bouteilles de 1L, 1,5L ou 2L. Cependant, il est important d’utiliser 4 bouteilles de même volume. Ceci afin que l’eau une fois s'être écoulée, soit équilibrée dans les bouteilles du bas et ne déborde. Le système de goulot des bouteilles inférieurs, contient une arrivée d’aire et n’est donc pas à 100% étanche. 

- Afin d'obtenir un meilleur écoulement et une quantité d'eau similaire dans les bouteilles, il est conseillé de placer le filtre sur une surface plane.

- Il est déconseillé de remplir les bouteilles à ras bord, car il y a un risque de fuite d'eau du à la brèche d'arrivée d'air. 

- Les UV-C ne passent pas à travers le plastique (PET) des bouteilles. Seulement, elles sont hautement mutagènes et agressent les tissus des yeux et de la peau ! Il important d'allumer la lampe quand vous n'êtes pas en contact direct avec les ondes (peau et yeux). 

- L’impression du filtre doit être fait avec du ASA, qui résiste aux UV. 

- Le filtre fonctionne que dans un sens. Si l'on veut effectuer un second filtrage avec la même eau dans les mêmes bouteilles. Il faut inverser les bouteilles pour réeffectuer le filtrage. 

------------------------------

<h3>2.	Pieces à acheter:</h3>

|    |    |
| --- | --- |
| ![ampoule](images/ampoule.PNG) | - Lampe UV-C Söll   (9W 12V)    (+- 18 Euro) 
| ![Quartz](images/quartz.PNG) | - Quartz                        (+- 4  Euro) |

**Outils nécessaires:**

-	Préservatif ou sac en plastique

-	Vaseline ou substance graisseuse : Hydrophobe

-	Powerbank

-	Cables + Cutter + outils mécaniques

-	4 Bouteilles en plastique (PET) identiques

------------------------------

<h3>3.	Fichiers à télécharger:</h3>

[P1](STL/P1.stl)  --- [P2](STL/P2.stl) ---  [P3](STL/P3.stl) ---  [P4](STL/P4.stl)

![axo1](images/axo1.png)


**P1 - 4 fonctions:**

     1: Il y a un espace interieur qui peut contenir le ballast et le stepup converter.

     2: Il y a une douille qui tient l'ampoule horizontalement, suivi de deux orifices de 4mm de diamètre. Ces ouvertures laissent passer les cables electriques.

     3: La surface plate exterieur a comme fonction de maintenir, stabiliser et fixer par la suite, la "Powerbank" sur le filtre.

     4: En vissant la P1 dans P2, elle compresse le quartz contre l'étanchéité, et rend étanche la chambre à UV. 

**P2 - 3 fonctions:**

     1: Le coté gauche a le meme diamètre que le quartz. Il tient donc le quartz afin qu'il ne flotte pas dans la chambre à UV. 

     2: Le deuxième diamètre (plus large), est crée une ouverture de 5mm entre la paroi et entre le quartz. C'est dans cet espace que l'eau circule.

     3: Les orifices (superieur et inférieur) ont différent formes et tailles. L'orifice supérieur est rond et d'un diamètre maximale possible. De cette façon l'eau rentre dans la chambre à UV, meme avec une sous-pression présente dans la bouteille. L'orifice inférieur est de forme ovale (3mm x 6mm). Le mechanisme qui tient la bouteille PET, contient un deuxième petit orifice rond de 2mm de diamètre. Cette arrivée d'air garanti un flux d'eau constant de 90 secondes.

**P3 - 2 fonctions:**

     1: P3 se raccorde avec la P2 et la P4. (distance imposé a cause de la taille du quartz)

     2: dans l'ensemble, P3 offre un bon appui-main pour manipuler l'objet pendant l'operation. 

**P4 - 2 fonctions:**

     1: du coté gauche de l'axonométrie de P4, il y a des appuis pour le quartz, pour éviter une flexion. 

     2: Sinon fonctions identiques à P2

------------------------

<h3>4. Le montage de l'objet:</h3>

|    |    |
| --- | --- |
| ![chambre_2sur3](images/chambre_0sur3.jpg) | <p>1: Il faut prendre les pièces P2, P3, P4 et bien enlever toutes les structures de soutien, après l'impression 3D. <p><p>2: Avec de la vaseline, ou autre substance hydrophobe, il faut enduire les systèmes de vissage pour les rendre étanche. <p><p>3: Il est conseillé de visser et dévisser légèrement les pièces avant de les enduire de vaseline, afin d’ajuster au mieux les pièces. <p><p>Si deux pièces sont coincées l'une dans l'autre. Utilisez un peu de savon avec de l'eau chaude pour aider la dilatation du plastique. Mais attention, car la force humaine peut aussi déformer les pièces imprimées. <p>  |

|    |    |
| --- | --- |
| ![chambre_2sur3](images/chambre_2sur3.jpg)![chambre_et_vaseline](images/chambre_et_vaseline.jpg) |  <p>1: Une fois les pièces vissées, il faut enlever l'excédent de vaseline. <p><p>2: Dès que les 3 pièces sont assemblées, faites attention que les "têtes de bouchons" soient correctement alignées. Assurez-vous que les mêmes orifices soient placés du même coté. <p>  |
 
|    |    |
| --- | --- |
| ![Quartz](images/quartz_et_préservatif.jpg) | <p>1: Pour étanchéifier l'espace entre le quartz et la chambre, munissez-vous d’un préservatif dont le bout est sectionné. L’anneau de celui-ci doit être posé au bout du bord incurvée du quartz. <p><p>(Un petit sac en plastique enroulé fonctionne aussi --> sous pression) <p><p>2: Le quartz est un matériau permettant de laisser passer les UV-C. Pour être le plus efficace possible, il doit être le plus propre possible. Conseil: nettoyez le à l'alcool fort, pour augmenter son rendement. <p> |
 
|    |    |
| --- | --- |
| ![etanchéité](images/chambre_et_preservatif.jpg) ![detail](images/detail_pression.jpg) | <p>1: Une fois le quartz correctement nettoyé, il peut être inséré délicatement dans la chambre à UV. <p><p>2: La pièce P1 va ensuite presser le quartz contre le préservatif et la coque. <p><p>3: La pièce P1 fonctionne aussi comme douille. Elle tiendra l'ampoule horizontalement dans le quartz. <p> |
  
------------------------

<h3>5. Circuit électronique:</h3>
