# TUTORIAL
# (OCOW) - ONE CHAIR ONE WORLD



### MATERIALS:
##### STRUCTURE

<br><img src="docs/final_projects/One-Chair-One-World/images/banner_acero_corrugado.jpg" width="60%">

<ol><li>Corrugated steel bar (Diam 10-12mm)</li></ol>

##### ENVELOPE (TO CHOOSE)

<br><img src="docs/final_projects/One-Chair-One-World/images/Captura_de_pantalla_2020-01-10_a_las_6.45.58.png" width="70%">

<ol><li>Textile thread</li><li>Vegetal thread</li><li>Synthetic thread</li></ol>

### TOOLS:

<br><img src="docs/final_projects/One-Chair-One-World/images/sierra-metal-300-mm-dexter-pro-8118755-1.jpg" width="30%"> <img src="docs/final_projects/One-Chair-One-World/images/1-2-6-electrical-metallic-tubing-using.jpg_350x350.jpg" width="30%">

<ol><li>Saw</li><li>Metallic pipe (30-40cm)</li></ol>

### FABRICATION:
##### MANUAL

<br><img src="docs/final_projects/One-Chair-One-Word/images/300-mm-dexter-pro-8118755-1.jpg" width="50%">

##### PROCESS

https://we.tl/t-1RSdZc3AAT