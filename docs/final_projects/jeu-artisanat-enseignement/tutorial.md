# Game, craft and teaching

## Tutorial

In response to this, there is an interest in making a handcrafted game that every Cuban could have at home and make it easily for children.

### Our goals:
- To use vegetable waste (vegetal fiber: sugar cane bagasse) and thus obtain an alternative solution to plastic toys.

### Create a game that allows the child :
- To create it himself ( or with the help of an adult )
- To use food waste and see it as a material that has potential in the manufacture of objects.
- To become familiar with geometric shapes
- To be imaginative and creative
- To solve problems
- To play with in different ways
- To assemble / disassemble at will
- To create as many pieces as he wishes
- The ideal toy imagined is a ball course that will be made with the recipe of bagasse and potato starch. The ball track will be composed of a repetitive module that will be made from a bagasse dough moulding process inside a mould made for.

![85](images/85.png)

### Recipe 1: Bagasse

#### Ingredients:
- Sugar cane
- Knife
- Cutting table
- Container

#### Prep:
### Cutting the sugar cane
- Cut a first portion of sugar cane
- Peel off the outer skin, which is hard and fibrous.
- Cut the portion in two pieces
- Cut into smaller portions

##### Extraction of sugar cane juice
- First possibility is to pass it through a juice extractor.
- The second possibility is to chew the small pieces of sugar cane previously cut in order to absorb all the juice.

### Getting the dried bagasse
- Once the juice has been completely removed, let the fibres rest to dry.
- Drying can be done in the open air for several days, or more quickly with a lighted radiator or over a chimney.

|![53](images/53.png)|![54](images/54.png)|![55](images/55.png)|![56](images/56.png)|
| --- | --- | --- | --- |

### Recipe 2 : Bagasse dough + potato starch

#### Ingredients:

- 1 portion of bagasse
- 4 portions of water
- 1 portion of potato starch
- 1 serving of vinegar
- Blue dye ( not required )

#### Prep:

- In a saucepan pour the water, bagasse and vinegar over low heat.
- When the bagasse is softer, pour in the potato starch gradually.
- Once the dough has become gelatinous, turn off the heat.
- Allow to cool
- Pour the dough into a mould

![dessin](images/dessin.jpg)

|![79](images/79.png) | ![80](images/80.png)|
| --- | --- |

| |Mold|Odour| Hardness| Flexibility| Touchability| Colour|Transparency| Moulding| Drying Time|
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
|Potato starch |not |pleasant |+++++ |+++ |pleasant |green |not |easy |5 hours|

**Notes:** The texture and hardness of the mixture depends on the thickness of the object during moulding. The thinner the object, the more brittle and less flexible it will be. The recipe for potato starch gives satisfactory results to the equivalent of the recipe with agar agar.

### Recipe 3 : Potato starch

#### Ingredients:

- 1 kg of potatoes
- Mixer
- 500 ml of water
- Filter

#### Prep:

- Peel the potatoes to remove the fibrous part.
- Cut the potato into small pieces
- Pour the potato cubes into a blender with the water.
- Mix until homogeneous mixture is obtained
- Filter several times and then let the liquid obtained rest
- Pour the liquid deposit on a cellophane . The deposit corresponds to the starch
- Allow the starch to dry to a dehydrated texture that corresponds to potato starch.

|![81](images/81.png)|![82](images/82.png)|![83](images/83.png)|![84](images/84.png)|
| --- | --- | --- | --- |

**Notes:** For 1 kilogram of potato, we get 80 grams of potato starch.
Potato starch is the starch extracted from potato tubers. It is used in technical applications such as wallpaper glue, fabric finishes and coatings, paper coating and coating, and as an adhesive in paper bagging and gummed paper tapes.

## Craft prototype

|![86](images/86.png)|![87](images/87.png)|![88](images/88.png)|
| --- | --- | --- |

## Prototype 1 digital mold

![89](images/89.png)

#### What's not working
A first attempt at a three-compartment mould, which was not very conclusive. Once we had put the dough inside the mould because we couldn't extract the dough without damaging it.

## Digital prototype 2 of the mould
We decided to make openings to facilitate the extraction of the dough.

[Tuto Video](https://www.youtube.com/watch?v=bIEQBOz_mDc)
<br>[STL File](STL/moule.stl) - [STL File Cover](STL/moulecover.stl)

|![90](images/90.png)|![t1](images/t1.png)|
| --- | --- |
|![t2](images/t2.png)|![t3](images/t3.png)|
|![t4](images/t4.png)|![t5](images/t5.png)|
|![t6](images/t6.png)|![t7](images/t7.png)|
|![t8](images/t8.png)|![t7](images/t9.png)|
|![t10](images/t10.png)|![t11](images/t11.png)|
|![t12](images/t12.png)|![t13](images/t13.png)|
|![t14](images/t14.png)|![t15](images/t15.png)|
|![t16](images/t16.png)|![t17](images/t17.png)|
|![t18](images/t18.png)|![t19](images/t19.png)|
|![t20](images/t20.png)|![t21](images/t21.png)|
|![t22](images/t22.png)|![t23](images/t23.png)|
|![t24](images/t24.png)|![t25](images/t25.png)|
|![t26](images/t26.png)|![t27](images/t27.png)|

![moule](images/moule.JPG)

## Mounting the handcrafted prototype with copper wire rails

![91](images/91.png)

## Mounting the digital prototype with printed rails 

[Tuto Video](https://www.youtube.com/watch?v=HV4JXKfIEq0)
<br>[STL File](STL/rail.stl)

|![rail](images/rail.png) |![r1](images/r1.png)|
| --- | --- |
|![r2](images/r2.png)|![r3](images/r3.png)|
|![r4](images/r4.png)|![r5](images/r5.png)|
|![r6](images/r6.png)|![r7](images/r7.png)|
|![r8](images/r8.png)|![r7](images/r9.png)|
|![r10](images/r10.png)|![r11](images/r11.png)|
|![r12](images/r12.png)|![r13](images/r13.png)|
|![r14](images/r14.png)|![r15](images/r15.png)|
|![r16](images/r16.png)|![r17](images/r17.png)|

![92](images/92.png)

## Final

![final](images/final.JPG)

## Other example of casting

![93](images/93.png)