<h1> Attelle gonflable <h/1> 
<h2> Les objectifs <h/2>

### 1. Aider à la prise en charge d'un blessé.

L'objectif principal de l'attelle gonflable est d'aider aux premiers soins d'un blessé. Pour ce faire il est important de pouvoir intervenir rapidement sans pour autant aggraver la blessure. L'objet à pour principale qualitée d'être réutilisable et de fonctionner grâce à la force de l'air.
Lors de la réalisation, il est donc très important de choisir les bons matériaux et faire un travail d'assemblague précis pour augmenter la qualité du produit ce qui permettra d'allonger sa durer de vie.

### 2. Fabrication facile pour tous.

Pour repondre aux besoins d'intervenir rapidement lors d'un accident impliquant une blessure interne, il est important que l'attelle se trouve à portée de main partout. Pour de faire l'objet doit être peu encombrant et facile à construire pour des personnes sans formation dans ce domaine.
La reflexion s'est donc portée sur la facilité de production de l'attelle. En utilisant le moins de matériaux possible et des matériaux facile à travailler et manipuler, une partie de l'objectif était atteint. Il est important par ailleurs, que la création du produit ne nécessite pas d'avoir recours à des machines consomatrice d'énergie. La mise en place doit donc être si facile qu'elle est possible par l'unique force des mains.

### 3. Coût de fabrication très faible.

Pour que l'attelle soit accessible rapidement, le produit doit être présent dans le plus de ménage possible. L'objectif d'avoir un prix très faible ou nul est donc très important. Pour répondre à cette demande de prix, il faut trouver des matériaux présent en très grande quantitée sur l'île. Des matériaux que l'on trouve chez soi ou son voisin ou encore facilement dans en magasin. Pour assurer des coûts faibles, les matériaux doivent, en plus d'être très peu rare, être solide pour ne pas devoir les remplacer à chaque utilisation. 




<h2> Les matériaux <h/2>

### 1. Chambre à air

| **Avantages** | **Inconvéniants** |
| ------ | ------ |
| Présent en grande quantité sur l'île | Nécessite une protection contre les objets coupants |
| Réparation aisée | | 
| Elasticité importante |  | 
| Travail à la main facile |  |
| Technologie déjà installée (pipette) |  | 

### 2. Filet 

| **Avantages** | **Inconvéniants** |
| ------ | ------ |
| Présent en grande quantité sur l'île | Fragilité due aux vides | 
| Elasticité permettant un confort suplémentaire | Réparation qui demande du temps et des matériaux supplémentaires  |
| Travail d'assemblage simplifié |  | 
| Respirabilité du membre | | 

<h2> La conception <h/2>

Suite aux recherches sur les attelles gonflables existantes, on remarque que pour que les nombreuses qualitées de ces attelles fonctionnent, la repartition de la pression de l'air doit être homogène sur tout le long du membre.
Pour que la pression soit la mieux repartie, la construction de l'attelle demande un travail un peu plus élaboré. En effet, la première étape pour une bonne repartition de la pression se fait lors de la fixation du boudin sur le filet. Le placement doit se faire de façon précise pour éviter un surplus de pression à un endroit et risquer d'endomager les chambres à air. Pour se faire, l'espace entre les boudins verticales doit etre égal sur tout le long du filet. De plus, les angles formés par les changements des directions ne peuvent pas être inférieur à 90°.
La forme d'une jambe ou d'un bras n'était pas droit et sans angle, pour que le pression soit uniforme sur le dessus comme le dessous, il est important de ressérer le bas soit à l'aide de sangle, soit en créant une forme d'entonnoire directement sur le filet en coupant des triangles dans le bas.
L'air doit pouvoir circuler librement lors du gonflement des chambres à air. Pour que la circulation soit fluide, les attaches servant à mantenir les chambres à air sur le filet doivent être assez lâche pour que les boudins puissent se gonfler sans rencontrer d'obstacle. 

<h2> Le tutoriel <h/2>

### - Etape 1 : Assemblage 

Cette étape consiste en l'assemblage des deux chambres à air pour en faire une seule avec une seule entrée et une seule sortie pour l'air.
A l'aide de papier collant resistant, on peut assembler les deux chambres à air après avoir coupé la partie sur laquelle se trouve la pipette d'une des deux chambres.
Les deux extrémités doivent elles aussi être rendue étanche en repliant les bords toujours à l'aide de papier collant. 
Une fois que le boudin est créé, il faut le placer de manière régulière sur le long du filet lui même placé dans le sens paysage. Il faut garder une marge sur le filet et donc ne pas aller de bout en bout avec le boudin. La partie sans boudin servira à la fermeture.

### - Etape 2 : Mise en place

Une fois que le boudin est placé le long du filet, on peut refermé le système de façon à ce que la partie gonflable soit à l'intérieur. Pour s'assurer de la résistance, il faut laisser un espace sans boudin sur le filet qui servira uniquement à la fermeture. 
Une fois que le produit à une forme cylindrique, il faut placé les sangles de serrage. Une au centre et une tout en bas ce qui donnera la forme d'entonnoire voulue. 

### - Etape 3 : Utilisation

Pour utilisé le produit fini, rien de plus simple. Il suffit d'enfiler l'attelle, vérifier qu'il n'y ai pas de plis dans les boudins et gonfler à l'aide d'une pompe.