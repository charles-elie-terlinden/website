# Make a 5V emergency generator (EN):

![image online](images/tuto.gif)

### Difficulty level :

Does not require a lot of know-how, but a lot of patience and precision.

### Necessary material :

- 24 nuts
- 20 washers
- 35 cm of threaded rod
- wooden board: 1500 cm2 + 650 cm2 + 60 + 60 + 60 + 60 + 1300 cm2 = 3690 cm2 of wood
- 10 screws
- a little glue
- 6 bearings (can be picked up on a skateboard)
- a 15 cm end of tube or motorcycle hose
- 2 glass syringes of 20 mL
- 2 small bottles with a cap the size of the pistons of the glass syringes
- 4 neodymium magnets, of substantial size.
- 4 copper coils with very fine wire
- a 5V-1A diode bridge
- 20 cm of cable
- a small alcohol burner: small glass jar with 100% cotton wick soaked inside

### Manufacturing steps:

Make the central support piece. To do this, make a hole of Ø 2.2 cm and a notch of the same thickness as that of the wooden board, like this:

![image online](images/centre.png)

Screw the central support piece onto the base plate, measuring 50 by 30 cm. For more stability, use glue.

![image online](images/2.png)

Insert 2 bearings in the hole of the central support piece. Slide in a 10 cm threaded rod, a nut on both sides, the previously cut wheels (with a diameter of 20 cm), and nuts to close everything.

![image online](images/3.png)

Make 2 connecting rods, dimensions as follows. Slide 1 bearing into each hole of each connecting rod.

![image online](images/bielle.png)

Attach the connecting rods as close as possible to the central axis of each wheel. To do this, make a hole on the wheels, slide a 6 cm threaded rod into it, with a nut inside the wheel, a nut outside the wheel, the connecting rod, then another nut.

![image online](images/4.png)

Make the syringe support parts, to the following dimensions:

![image online](images/support.png)

Fix them on the central support piece using screws (and glue, if available).

![image online](images/5.png)

Slide the syringes between the two support pieces, surround them with a soft thermofuge fabric, lock them in place with 4 threaded rods of 6 cm, and nuts.

![image online](images/6.png)

Make the connecting piece between the syringes and the connecting rods. To do this, cut the top of a bottle, make a hole in the cap the size of the piston ring, two holes on the sides of the size of a 6 cm threaded rod, as follows:

![image online](images/support_bielle.png)

Slide the pistons into the bottles, and the bearing of the connecting rods into the threaded rods. Chock everything with washers, and close the assembly with nuts.

![image online](images/7.png)

Attach the two syringes using the hose or plastic tube available. If necessary, improve the seal using a wire.

![image online](images/8.png)

Slide the magnets into the slots left in the wheels. If necessary, secure it between the magnets and the wheel using adhesive tape.

![image online](images/9.png)

Make the burner and install it under one of the syringes. The burner is simply a small glass container, like a jar, which is filled with rubbing alcohol, in which a cotton wick of about ten cm is dipped, which is passed through a hole in the lid.

![image online](images/10.png)

Place the 4 coils on the central support piece, immobilize them with wire, tape or screws.

![image online](images/11.png)

Connect the coils in series to each other, then solder them on a 5V and 1.25A diode bridge. Solder the wiring on the diode bridge, and attach a female USB plug to the wiring by soldering the two.

![image online](images/modele_fin.JPG)

### How to use it ?

To start the generator, turn on the alcohol burner, manually start one of the wheels clockwise. Connect a powerbank or a mobile phone to the electrical system.

<iframe width="560" height="315" src="https://www.youtube.com/embed/nOqqSy_ClCY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### What are the precautions to take when making the engine ?

It is essential to use good bearings and good quality syringes with as little friction as possible (to ensure that these are correct, rotate the plunger in the syringe: it should continue to rotate by itself if the syringe is good). Also, for maximum yields, use neodymium magnets, not graphite, which are weaker. The copper coils should be made with very fine, insulated wire, and not present any imperfection, at the risk of causing short circuits which could sabotage the results.

________________________________

# Fabriquer un générateur d'urgence 5V (FR) : 

![image en ligne](images/tuto.gif)

### Niveau de difficulté : 

Ne nécessite pas énormément de savoir-faire, mais beaucoup de patience et précision. 

### Matériel nécessaire : 

- 24 écrous
- 20 rondelles
- 35 cm de tige filettée
- planche de bois : 1500 cm2 + 650 cm2 + 60 + 60 + 60 + 60 + 1300 cm2 = 3690 cm2 de bois
- 10 vis
- un peu de colle
- 6 roulements (peuvent être récupérés sur un skateboard)
- un bout de tube ou de durite de motocyclette de 15 cm
- 2 seringues en verre de 20 mL
- 2 petites bouteilles avec un bouchon de la taille des pistons des seringues en verre
- 4 aimants en néodyme, de taille conséquente.
- 4 bobines de cuivre avec du fil très fin
- un pont de diode 5V-1A
- 20 cm de cable
- un petit bruleur à alcool : petit bocal en verre avec mèche 100% coton trempée à l'intérieur

### Etapes de fabrication : 

Réaliser la pièce de support centrale. Pour ce faire, faire un trou de Ø 2,2 cm et une encoche de la même épaisseur que celle de la planche de bois, comme ceci : 

![image en ligne](images/centre.png)

Visser la pièce de support centrale sur le plateau de base, d'une dimension de 50 par 30 cm. Pour plus de stabilité, utiliser la colle.

![image en ligne](images/2.png)

Insérer 2 roulements dans le trou de la pièce de support centrale. Y glisser une tige filettée de 10 cm, un écrou de part et d'autre, les roues découpées préalablement (d'un diamètre de 20 cm), et des écrous pour clore le tout.

![image en ligne](images/3.png)

Réaliser 2 bielles, de dimensions comme suit. Glisser 1 roulement dans chaque trou de chaque bielle. 

![image en ligne](images/bielle.png)

Fixer les bielles au plus près de l'axe centrale de chaque roue. Pour ce faire, réaliser un trou sur les roues, y glisser une tige filettée de 6 cm, avec un écrou à l'intérieur de la roue, un écrou à l'extérieur de la roue, la bielle, puis encore un autre écrou. 

![image en ligne](images/4.png)

Réaliser les pièces de support des seringues, aux dimensions suivantes : 

![image en ligne](images/support.png)

Les fixer sur la pièce de support centrale à l'aide de vis (et de colle, si disponible).

![image en ligne](images/5.png)

Glisser les seringues entre les deux pièces de support, les entourer d'un tissu mou thermofuge, les verrouiller à leur place à l'aide de 4 tiges filettées de 6 cm, et d'écrous. 

![image en ligne](images/6.png)

Réaliser la pièce de jonction entre les seringues et les bielles. Pour ce faire, couper le haut d'une bouteille, faire un trou dans le bouchon de la taille de la bague du piston, deux trous sur les côtés de la taille d'une tige filettée de 6 cm, comme suit : 

![image en ligne](images/support_bielle.png)

Glisser les pistons dans les bouteilles, et le roulement des bielles dans les tiges filettées. Caler le tout avec des rondelles, et clore l'ensemble avec des écrous.

![image en ligne](images/7.png)

Joindre les deux seringues à l'aide de la durite ou du tube plastique dont on dispose. Si nécessaire, améliorer l'étanchéité à l'aide d'un fil de fer.

![image en ligne](images/8.png)

Glisser les aimants dans les fentes laissées dans les roues et s'assurer de respecter la polariter des aimants, Nord d'un coté et Sud de l'autre. Si nécessaire, assurer la fixation entre les aimants et la roue à l'aide de ruban adhésif. 

![image en ligne](images/9.png)

Réaliser le bruleur et l'installer sous une des seringues. Le bruleur est simplement un petit récipient en verre, de type bocal, qu'on remplit d'alcool à bruler, dans lequel on trempe une mèche de coton d'une dizaine de cm, que l'on fait passer à travers un trou dans le couvercle.

![image en ligne](images/10.png)

Placer les 4 bobines à 90° les unes aux autres sur la pièce de support centrale, les immobiliser avec du fil de fer, du ruban adhésif ou des vis. 

![image en ligne](images/11.png)

Relier les bobines en série 2 à 2, puis les souder sur un pont de diode 5V et 1,25A. Souder du cablage sur le pont de diode, et joindre une prise USB femelle au cablage en soudant les deux. 

![image en ligne](images/modele_fin.JPG)

### Comment l'utiliser ? 

Pour lancer le générateur, allumer le bruleur à alcool, lancer manuellement une des roues dans le sens horaire. Brancher une powerbank ou un téléphone portable en sortie du système électrique.

<iframe width="560" height="315" src="https://www.youtube.com/embed/nOqqSy_ClCY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Quelles sont les précautions à prendre lors de la réalisation du moteur ? 

Il est fondamental d'utiliser de bons roulements et des seringues de bonne qualité avec le moins de frottement possible (pour s'assurer que celles-ci sont correctes, faire pivoter le piston dans la seringue : celui-ci devrait continuer à tourner tout seul si la seringue est bonne). Aussi, pour un maximum de rendements, utiliser des aimants en néodyme, et non en graphite, qui sont moins forts. Les bobines de cuivre devraient être faites avec du fil très fin, isolé, et ne présenter aucune imperfection, au risque d'avoir des courts-circuits qui pourraient saboter les résultats. 
