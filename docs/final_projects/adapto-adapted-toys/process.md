### Mercredi 27 novembre

N’ayant toujours pas de sujet concret, nous nous rencontrons pour pousser nos recherches plus loin et pour trouver une idée plus concrète et surtout plus complète. C’est à ce moment qu’on a pensé aux jouets adaptés pour les personnes porteuses de handicap. Karine travaille dans un camp avec des enfants et des adultes handicapés (des déficiences mentales au handicaps moteurs). Ils utilisent des outils tous les jours pour aider les jeunes et moins jeunes à évoluer au sein du camps. Certains ont besoin de garder leur calme, d’autres doivent se concentrer, apprendre à tenir un crayon, etc.


« Balles de stress, objets de caoutchouc à mordiller, bracelets de plastique à déformer : ces outils permettent notamment à des personnes atteintes d’une affection comme un trouble déficitaire de l’attention avec hyperactivité (TDAH) ou un trouble du spectre de l’autisme de s’« autoréguler », et d’être disposées à écouter ou à travailler. »


Suite à cette idée, nous contactons directement des groupes de parents sur facebook cherchant plus d’informations sur l’autisme et/ou d’autres déficiences intellectuelles.

Ayant un jeu d’échec portatif pour aveugle en notre possession, il à été plus simple d'étudier un objet déstiné aux personnes porteuses de handicaps. Les pièces sont encastrées dans la plateforme de jeu pour empêcher qu’elles se déplacent lorsque mon grand-père passait ses mains pour sentir où elles se trouvaient. De plus, les cases blanches et noires sur la planche de jeu n’avaient pas la même texture et les pions blancs et noir non plus. Les blancs étant un peu plus pointu et les noirs étant lisse.

![Echec](images/jeu_echec_aveugle.png)


### Vendredi 29 novembre

Depuis notre dernière rencontre, nous avons eu une réponse sur un groupe de mère ayant un ou plusieurs enfants autistes ou ayant un TDAH (trouble de déficit d’attention avec hyperactivité).

![Groupe facebook](images/Groupe_facebook.png)

![Traduction facebook](images/Traduction_facebook.png)

Avec cette réponse en tête, nous rencontrons Victor et Denis pour leur parler de notre problématique et de notre idée de créer une gamme d’outils adaptés aux personnes ayant un handicap. La problématique semble être la bonne pour nous et le projet est dans notre domaine de compétence.


## Semaine 10

### Mardi 3 décembre

Avant de retourner en atelier le mardi 3 décembre, nous avons discuté avec une femme sur les besoins éventuels à Cuba. L’information semble très peu disponible et les diagnostics sont difficiles à obtenir. Les cas plus évidents d’autisme et de trisomie sont facilement identifiables alors que les cas plus légers de déficience mentale, de TDAH ou de trouble d’apprentissage sont plus difficile à évaluer et les experts sont rares. Les parents doivent donc se fier à leur propre jugement et aux informations qu’ils trouvent sur internet pour aider leur enfant à s’adapter le mieux possible.

En atelier nous décidons d’explorer plusieurs pistes possibles en utilisant que du matériel de bricolage. Parallèlement nous commençons l’impression de pièces pour notre outils d'aide au contrôle de l'agitation avec trois jonctions différentes, faute d’en avoir un vrai avec nous pour prendre des mesures.


__Outils d'aide au contrôle de l'agitation:__

![pieces tangle impression 3D](images/pieces_sur_imprimante.jpg)

![pieces tangle comparaison](images/pieces_imprimees.jpg)

__Règle de lecture :__

![Regle de lecture](images/regle_de_lecture.jpg)

__Dominos :__

![Dominos](images/dominos_tactils.jpg)

__Outil d’éveil à la motricité :__

![Outil ecriture](images/jeux_devail.jpg)


### Vendredi 29 novembre

__Pré-jury__

Les commentaires ont été très constructifs. Nous devons faire attention au brevet potentiel qu’il pourrait y avoir pour le tangle et la gamme de produit doit être clarifiée. Mis à part ça, nous sommes sur la bonne piste et d’autres tests sont à prévoir pour créer des objets facilement reproductibles à Cuba.



## Semaine 11

### Mardi 10 décembre

Nous avons avons fait un petit contre-rendu d’où nous en étions après ce premier pré-jury et où nous voulions aller pour le prochain et éventuellement pour la remise finale. Je vais continuer les pièces d'outils d'aide au contrôle de l'agitation à imprimer, pour améliorer la connexion entre les pièces et nous permettre d’en avoir un qui fonctionne le plus rapidement possible. Une fois les connexions fonctionnelles, nous pourrons ensuite créer des textures et nous amuser avec les couleurs.

Pour le travail, l'un devra modéliser les planches d’éveil à l’écriture pour les découper à la CNC et les pièces de dominos concaves pour aussi les découper à la CNC. Une fois les pièces de dominos modélisées, l'autre prendra le relais pour les pièces convexes qui seront imprimées à l’imprimante 3D.

Pour la présentation, nous voulons aussi améliorer notre discours et notre présentation visuelle. Comme le but est de faire un Pecha Kucha nous voulons faire une présentation graphique sans texte où seule les images parlent d’elles-mêmes.

_Le Pechakucha ou Pecha Kucha (du japonais ペチャクチャ : « bavardage », « son de la conversation ») est un format synchronisant une présentation orale à la projection de 20 diapositives se succédant toutes les 20 secondes, de préférence sans effets d'animations. La présentation dure ainsi exactement 6 minutes et 40 secondes. Ce format impose de l'éloquence, un sens de la narration, du rythme, de la concision, tout autant que de l'expression graphique._


### Vendredi 13 décembre

Cette semaine, nous avons modifié les dimensions des pièces de tangle et la forme des connexions. Nous avons aussi imprimé les nouvelles pièces et le résultat était très positif. Les pièces s’emboitent bien avec très peu de modification. Il suffit simplement de poncer un peu le trou, enlever une couche de plastique à peine. Nous avons imprimé des pièces à 15% de remplissage avec des supports pour la partie convexe de la connexion. D’autres pièces ont été imprimé à 50% de remplissage et sans support et le dernier test était à 100% de remplissage sans support. Les temps d’impression étaient tous très semblables autour de 35 minutes pour 3 pièces. En voyant que tous les tests fonctionnaient, mais que les supports semblaient apportés une meilleure adhérence au plateau, nous avons imprimé 12 pièces supplémentaires à 20% de remplissage avec des supports.

![Pieces tanlge](images/tangle2.1.jpg)

Comme les pièces d'outils d'aide au contrôle de l'agitation sont petites, nous avons imprimé 6 pièces à 200% pour bien voir les détails de celles-ci et mieux comprendre le fonctionnement et les erreurs.

![tanlge geant](images/tangle3.0.jpg)

Pour la suite, il faut encore modifier un peu la connexion pour que ça soit plus solide et créer des textures et un nouvel objet qui n’est pas presqu’identique au tangle original.

Nous avons aussi travaillé sur les dominos sensoriels. Dans fusion nous avons modélisé les dominos concaves pour découper à la CNC avec des formes géométriques que nous pourrons colorier plus tard.

![domino concave](images/Domino_concave.png)

En même temps, nous avons commencé les dominos tactiles qui seront imprimés avec l’imprimante 3D. Pour ces derniers, nous imprimerons la base des dominos tous de la même couleur, puis des pièces avec différentes textures et différentes couleurs selon les textures, viendront s’imbriquer dans la base pour compléter les dominos sensoriels. Il faudra voir s’il vaut vraiment la peine, niveau temps d’impression et prix du filament, d’imprimer les pièces en 3D plutôt que de les faire autrement, voire de les acheter.

![domino convexe](images/domino_convexe.png)

Pour finir, nous avons commencé la documentation du travail final et pris quelques photos avec un beau fond et une belle luminosité. En voici le résultat :

![Gamme produit](images/Gamme2.0.jpg)
![synthese produit](images/image_synthese.jpg)



## Semaine 12

### Mardi 17 décembre

Comme vendredi nous voulons présenter des échantillons de nos différents tests, nous complétons les modélisations des pièces de dominos pour pouvoir en imprimer quelques-unes et en découper à la CNC.


### Mercredi 18 décembre

Nous avons imprimé des pièces d'outils d'aide au contrôle de l'agitation avec un petit « filet » sur la jonction pour obtenir la résistance désirée lorsque les pièces sont mises ensembles.

![Pieces avec rebord](images/Montage_pieces_filet.png)

Comme on peut le voir sur les images des impressions, le « filet » n’a pas été imprimé, j’imagine que c’est un détail trop petit pour être imprimé par cette machine. Comme nous avons atteint la limite de ce que l’imprimante 3D PLA peut nous offrir pour ces pièces-ci, nous devons réfléchir à soit adapté la pièce pour qu’elle soit imprimé et que les connexions fonctionnent bien ou utiliser une autre technique de fabrication comme l’imprimante résine ou le moulage.

Comme dernier test, nous avons imprimé les mêmes pièces avec le rebord à 200%. Le but étant de voir si, comme la première fois, on obtenait des résultats plus précis.

![Pièces 200%](images/outil d'aide au contrôle/test_3.jpg)

On peut voir qu’un petit rebord a été imprimé et à l’intérieur du trou, nous voyons aussi un creux où le rebord peut venir s’insérer et avoir une meilleure connexion.


### Vendredi 20 décembre

Lors du deuxième pré-jury, nous n’étions pas aussi avancés que nous l’aurions désiré, mais nos tests sur le tangle sont de plus en plus concluants et notre problématique nous tient à cœur. Les invités semblent avoir apprécié l’idée sans nécessairement comprendre le projet en entier. Nous voulons avoir des objets terminés pour le jury final.

Les commentaires ont été très constructifs. Nous pensons pouvoir faire évoluer le projet dans la bonne direction pour la suite.

## Semaine Charrette

### Lundi 6 janvier au jeudi 9 janvier

Dernière semaine…

Pendant les vacances j’avais commencé à modéliser une nouvelle connexion entre les pièces de tangle que j’ai pu tester à la première heure mercredi matin en imprimant de nouvelles pièces.

![nouvelle connexion](images/outil d'aide au contrôle/test_4.jpg)

Il y avait toujours un petit problème, donc après un petit ajustement de taille, nous avons enfin eu un résultat concluant!

![final connexion](images/outil d'aide au contrôle/test_5.jpg)
![final connexion](images/outil d'aide au contrôle/outil_agitation.jpg)

En même temps que nous imprimions les nouvelles pièces, nous avons eu l’idée d’un 4e objet qui pourrait s’ajouter à la collection d’Adapt’o. Une règle permettant d’écrire en braille pour faciliter l’inclusion des personnes malvoyantes. Pour le premier test, nous avons imprimé une règle qui n’était pas assez longue pour une page A4 normale.

![règle braille 1](images/Règle braille/regle_verte.jpg)

Pour la version 2.0 nous avons donc imprimé une règle plus longue avec des petites attaches aux deux extrémités. Malheureusement la première impression s’est un peu décollée et les attaches ne se sont pas bien imprimé.

![règle braille 2](images/Règle braille/regle_rouge.jpg)

Nous avons donc imprimé une deuxième fois la version 2.0, mais en utilisant un radeau, ce qui a été difficile à enlever au final, mais le résultat reste très satisfaisant.

![règle braille 3](images/Règle braille/regle_braille.jpg)

Pour ce qui est des dominos, nous voulions les découper à la CNC ou éventuellement à la lasercut, mais pendant les vacances j’ai demandé à un ami de la famille aveugle de tester nos 4 options et de me dire ce qu’il en pensait. Selon lui, la version où les formes sont convexes et la barre centrale concave est la plus prometteuse parce que sur les autres, il est difficile de bien lire les formes et de comprendre que la barre centrale ne fait pas partie du jeu. En arrivant pour les découper, nous avons réalisé que la plus petite fraise faisait 6 mm de largeur et que nos trous ne font que 2 mm de large, ce qui rend la découpe impossible. De plus, comme les formes sont convexes, il n’était plus possible d’utiliser la lasercut, nous avons donc dû utiliser l’imprimante 3D. Pour la première version, nous n’avons imprimé que les pièces avec les formes.

![domino 1](images/Domino/dominos_impression_1.jpg)

Le problème c’est que lorsqu’on joue aux dominos, on place les pièces faces vers le bas pour piocher. Dans ce cas-ci, il est possible de deviner de quelles formes il s’agit avant même de les piocher.

![domino 3](images/Domino/domino_tete_en_bas.jpg)

Nous avons donc modélisé un autre type de domino avec des parois surélevée.

![domino 2](images/Domino/domino_impression_2.jpg)

Après cette deuxième version, nous nous sommes rendu compte qu’il y avait une erreur dans la forme des dominos. La largeur du domino doit avoir la même dimension que la longueur d’une des faces du domino. De plus, nous voulions avoir des couleurs différentes pour chaque forme, il nous fallait donc une solution. Nous avons séparé les formes de la structure du domino pour pouvoir les imprimer avec des filaments différents.

![domino 4](images/Domino/Dominos.jpg)

Dans un dernier temps, nous avons continué le travail sur les planches d’éveil à l’écriture et à la motricité fine. Nous devions commencer par modéliser la planche et se familiariser avec la CNC. Au début les tests n’étaient pas concluants parce que notre modèle n’était pas bien paramétré. Nous avons fait plusieurs tests avant d’arriver à un résultat très satisfaisant.

![Eveil 1](images/planche éveil/planche_2.jpg)
![Eveil 2](images/planche éveil/planche_3.jpg)
![Eveil 3](images/planche éveil/planche_4.jpg)
![Eveil 4](images/planche éveil/planche_eveil.jpg)


Pour finir cette semaine de travail il ne restait plus qu’à préparer nos documents sur le Git et le site internet et à faire notre présentation pecha kucha.
