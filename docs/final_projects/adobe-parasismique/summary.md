# Adobe parasismique

## Summary

![image test](images/adobe.png)

### Question (EN): 

How do earthen constructions, a forgotten heritage, allow frugal self-construction processes with no need for recourse to the public or black market?

_Hypothesis :_

Earth is a locally sourced, locally available bio-material, with low gray energy and recyclable. These qualities make it a building material of the future. However, at present, its use in new constructions remains marginal. These raw earth construction techniques mobilize local labor while considerably reducing transport costs, both economically and environmentally. However, the earth can deform in compression and shear which makes it an interesting material for earthquake-resistant. As the earth does not have a high tensile strength associated with seismic risks, it is wise to associate it with wood, or bamboo. The goal of this project is to create an adobe mold and a seismic system thought together. In this way, the premises, when carrying out home extensions, will set up an earthquake-resistant system. Indeed, self-construction often does not support these considerations, leading to major risks of collapse which can be reduced with simple devices.

## Résumé

### Problématique (FR) : 

Comment les constructions en terre, patrimoine oublié, permettent des procédés d’auto-construction frugal n’ayant nul besoin d’un recours au marché public ou noir ? 

_Hypothèse :_

La terre est un matériau bio ressourcé, localement disponible, à faible énergie grise et recyclable. Ces qualités en font un matériau de de construction d’avenir. Pourtant à l’heure actuelle, son utilisation dans les constructions neuves reste marginale. Ces techniques de construction en terre crue permettent de mobiliser un main d’oeuvre locale tout en diminuant considérablement les coûts de transport, à la fois de manière économique et environnementale. Néanmoins, la terre peut se déformer en compression et cisaillement ce qui en fait un matériau intéressant pour le parasismique. Comme la terre n’a pas une forte résistance en tension associé aux risques sismiques, il est judicieux de l’associer au bois, ou le bambou. Le but de ce projet est de créer un moule adobe et un système parasismique pensé ensemble. De cette manière, les locaux, lors de la réalisation d’extensions d’habitations, mettront en place un système parasismique. En effet, l’autoconstruction ne prend souvent pas en charge ces considérations, entrainant des risques majeurs d’effondrement qui peuvent être réduits avec de simples dispositifs.t

## Resumen

### Problématica (ES) :

¿Cómo las construcciones de tierra, una herencia olvidada, permiten procesos frugales de autoconstrucción sin necesidad de recurrir al público o al mercado negro?

_Suposición:_

La Tierra es un bio-material de origen local, disponible localmente, con baja energía gris y reciclable. Estas cualidades lo convierten en un material de construcción del futuro. Sin embargo, en la actualidad, su uso en nuevas construcciones sigue siendo marginal. Estas técnicas de construcción de tierra cruda movilizan mano de obra local mientras reducen considerablemente los costos de transporte, tanto económica como ambientalmente. Sin embargo, la tierra puede deformarse en compresión y corte, lo que la convierte en un material interesante para resistir terremotos. Como la tierra no tiene una alta resistencia a la tracción asociada con riesgos sísmicos, es aconsejable asociarla con madera o bambú. El objetivo de este proyecto es crear un molde de adobe y un sistema sísmico pensado en conjunto. De esta manera, las instalaciones, al realizar las extensiones de la casa, establecerán un sistema resistente a los terremotos. De hecho, la autoconstrucción a menudo no respalda estas consideraciones, lo que conlleva riesgos importantes de colapso que pueden reducirse con dispositivos simples.