# Title of your project

## Processus de recherche

## Semaine 7 - vendredi

<a>La matinée on a determiné le sujet sur lequel on voulait travailler. Au début j'avais l'idée de faire un panneau solaire thermique, mais lors du workshop je me suis rendu compte que ça posait des problèmes par rapport à l'acheminement de l'eau, de la source au point de consommation, car cela nécessite de la tuyauterie, hors les tuyaux sont difficiles à se procurer et sont chers, il faut aussi un savoir faire de plombier. J'ai donc décidé de travailler sur le prototypes qu'on a réalisé lors du workshop et de l'améliorer. Ce prototype a plutôt bien fonctionné donc je trouve intéressant de poursuivre sont élaboration.
</a>

## Semaine 8 - mardi

<a>J'ai essayer d'identifier ce qui n'allait pas dans le prototype et dans les expérimentations qu'on avait réalisées. J'aimerais comprendre pourquoi l'effet de serre qu'on pensait mis en jeu dans le système n'a pas eut lieu. Peut-être qu'il faudrait faire une expérience avec des bouteilles en verre pour limiter les déperdition de chaleur des cannettes. J'ai aussi remarqué dans le site d'Ernesto Orosa qu'un ingénieur avait conçu un panneau solaire thermique avec des bouteilles en plastiques peintes en noir. Peut-être que je pourrais tester cette solution et voir si c'est mieux. 
<br>J'ai aussi identifier tous les types de miroirs qu'il faudrait expérimenter :
<br>- le miroir cylindro-parabolique
<br>- le miroir parabolique
<br>- le miroir concave
<br>- le miroir plat
</a>

## Semaine 8 - vendredi

J'ai parlé de mes idées d'expérimentation aux profs et finalement Victor m'a suggéré de trouver un système qui ne se contente pas de pasteriser l'eau mais qui peut tout pasteriser et permettre de faire des conserves. 
<br>Il fallait donc que je change de système. J'ai fait des recherches sur le procédé pour faire des conserves et j'ai trouvé dans le livre "El libro de la familia" des recettes pour faire des conserves. 

## Semaine 9 - mardi

<a>J'ai eut l'idée de faire des recherches sur les fours solaires en espagnol et j'ai trouvé des articles intéressants. J'ai recherché "horno solar" ce qui signifie "four solaire" et j'ai trouvé un four solaire qui ressemble à la bouilloire solaire mais qui sert à cuire des aliments, c'est un barbecue solaire qui fonctionne selon le même principe. Ce four est fort intéressant mais il ne permet pas de faire des conserves. [voir ce four solaire](https://articulo.mercadolibre.com.pe/MPE-432158109-horno-solar-100-eco-amigable-_JM?quantity=1)

<img src="img/journalBord/Florian-Aliker/horno-solar-100-eco-amigable-D_NQ_NP_784504-MPE27797210972_072018-F.jpg" width="40%">

Finalement j'ai trouvé le type de four solaire idéal pour tout pasteriser dans [un article espagnol](http://www.diariodemarratxi.com/se-presenta-en-baleares-un-proyecto-de-hornos-solares-eficientes/). Dans les îles Baléares (Espagne), sur l'île de Minorque, l'IBANAT (Instituto BAlear de NATuraleza = Institut Baléare de la Nature) a conçu un four solaire de type boite très efficace et polyvalent capable de faire bouillir de l'eau, de pasteuriser et cuire toutes sorte d'aliments. La température est montée jusqu'à 150°C. Les expérimentations ont été faites fin avril 2016, la température ambiante était de 18°C. <br>Leur projet s'appelle "Ecocina Solar", j'ai trouvé [la page Facebook du projet](https://www.facebook.com/pg/ecocina/posts/) et j'ai découvert qu'ils ont déjà fait des conserves grâce à ce four. En faisant des recherches sur ce type de four j'ai découvert qu'il s'inspire du four [Global Sun Oven](https://www.sunoven.com/around-the-world/benefits/), d'après [l'atlas de la cuisine solaire](https://www.atlascuisinesolaire.com/fonctionnement-et-caracteristiques-four-solaire.php) la température de ce four monte à 150°C en 30 min. Avec ce type de four on peut tout cuire mais il faut compter 3 fois plus de temps de cuisson qu'avec un four classique (électrique ou gaz). 

<img src="img/journalBord/Florian-Aliker/horno-solar-300x336.jpg">
<img src="img/journalBord/Florian-Aliker/horno1.jpg" width="40%">

<br> J'ai aussi trouvé de la documentation sur ce type de four :
<br>-[l'atlas de la cuisine solaire](https://www.atlascuisinesolaire.com/fonctionnement-et-caracteristiques-four-solaire.php)
<br>-[Four Atominique](http://four-solaire.iguane.org/)
<br>-[Tutoriel Fablabo](https://fablabo.net/wiki/Four_solaire_30%C2%B0)
<br>- [Bolivia Inti](http://www.boliviainti-sudsoleil.org/spip.php?article453)
<br>- [Solar Cookers International]( https://www.solarcookers.org/)
<br>- [Solar cookers Wiki](https://solarcooking.fandom.com/wiki/Category:Solar_cooker_plans)
<br>- [Tutoriel Low Tech Lab](https://wiki.lowtechlab.org/wiki/Four_solaire_%28cuiseur_type_bo%C3%AEte%29)
<br>- [Brochure Cuiseurs Solaires](https://www.pearltrees.com/s/file/preview/129215780/plansfr.pdf)

Ce four correspond parfaitement à la demande de Victor. Il m'a demandé si on pouvait fabriquer ce four avec les matériaux disponible à Cuba. Pour moi les miroirs peuvent être fait en récupérant le métal réfléchissant des canettes. Il m'a alors demandé de vérifier si ce type de four existait à Cuba. J'ai taper "horno solar en Cuba" et là je suis tombé sur ce même type de four -> [voir four solaire cubain](https://www.cubanet.org/mas-noticias/horno-solar-despierta-curiosidad-y-ahorra-dinero/). C'est assez démoralisant car finalement je n'apporte rien de nouveau, ce type de four existe déjà à Cuba. Cependant j'ai remarqué que les miroirs réflécteurs sont en faite un seul miroir conique ce qui peut être un problème car l'orientation des miroirs est très importante. Il faut que je fasse d'avantage de recherche pour savoir comment ce type de four est fabriqué à Cuba, avec quel matériaux ?

<img src="img/journalBord/Florian-Aliker/hornocubain.jpg">

</a>

## Semaine 09 - vendredi

<a>
En faisant de plus ample recherches sur les fours solaires à Cuba j'ai découvert qu'il existait le Groupe des Energie Renouvelables Appliquées = Grupo de Energías Renovables Aplicadas (GERA) en espagnol, fondé par le professeur de physique Jorge Bonzon de l'Université de l'Est (Universidad de Oriente) en 1997. Ce groupe fait des recherches sur les énergies renouvelables et fait la promotions des fours solaires. Le GERA a installé des fours solaires dans des lieux publics, principalement à Santiago de Cuba, pour faire connaitre ce procédé auprès de la population. Il organise aussi des activités pédagogiques auprès des enfants des écoles primaires pour les sensibiliser aux énergies renouvelables. Leur dernière activité semble dater de 2016.

<br>[Article SCI](https://solarcooking.fandom.com/wiki/Grupo_de_Energ%C3%ADas_Renovables_Aplicadas)
<br>[Rapport SCI de 2011](https://vignette.wikia.nocookie.net/solarcooking/images/6/65/Applied_Renewable_Energy_Group%2C_Cuba_-_Report_Summer_2011.pdf/revision/latest?cb=20110903185215)
<br> [Facebook du GERA](https://www.facebook.com/pages/category/Nonprofit-Organization/Grupo-de-Energ%C3%ADas-Renovables-Aplicadas-140669672633240/)

<br>
<img src="img/journalBord/Florian-Aliker/GERA1.jpg" width="25%">
<img src="img/journalBord/Florian-Aliker/GERA3.jpg">
<img src="img/journalBord/Florian-Aliker/GERA2.jpg" width="20%">

<br>Ce groupe a construit différents types de fours solaires mais diffuse principalement les fours solaires de types boites auprès de la population. On peut constater que ces fours boites "cubains" ont tous le même design avec un miroir fixe et conique. L'inconvénient du four solaire cubain est qu'on ne peut pas l'orienter perpenduculairement au soleil en fonction des saisons. Il est aussi très encombrant et la fabrication du miroir semble assez complexe (tole pliée...). Cependant ce four a très peu de défaut et je ne vois pas beaucoup d'améliorations à apporter à part corriger le problème d'orientation de la vitre et des miroirs.
<br> Victor m'a suggéré de déconstruire l'objet et d'identifier ces limites. Une des limites de ce système est que le four ne fonctionne pas sans soleil, il ne peut pas être utilisé pour cuisiner le soir.  En faisant des recherches j'ai découvert qu'il existait un four solaire qui fonctionne même le soir (voir [GoSun Grill](https://www.cuiseur-solaire.com/gosun-grill-four-solaire/) ) en stockant la chaleur dans une pile thermique. Cela pourrait être une amélioration significative.
</a>


## Semaine 10 - mardi

J'ai fait des recherches pour trouver un moyen de stocker la chaleur pour pouvoir cuisiner le soir. Ma première idée était d'emmagasiner l'énergie grâce à l'inertie thermique des matériaux, comme la brique. Il s'agit d'un stockage par chaleur sensible, mais ce principe est très long et peu efficace. J'ai découvert qu'il existait des matériaux capables de stocker beaucoup plus de chaleur pour un même volume : il s'agit des Matériaux à Changement de Phase (MCP). Les MCP sont des matériaux capables de changer d'état à des températures inférieures à 80°C. Le but est d'utiliser la chaleur latente de ces matériaux pour stocker plus d'énergie. La chaleur latente est l'énergie absorbée par un matériau lors de son changement d'état. Ces matériaux sont par exemple : l'eau, les sels fondus, la paraffine. 
<br>La paraffine est contenu dans les bougies, c'est donc un matériau facile à se procurer à Cuba. La paraffine fond à 50°C. Elle pourrait stocker la chaleur dans le four solaire. Pour limiter les déperditions il faudrait quelle soit confinée dans un espace isolé. Par conséquent on ne peut pas exposer directement la paraffine aux rayons du soleil. Il faudrait un fluide caloriporteur, comme l'eau, pour capter la chaleur et la transférer à la paraffine. L'idée serait de faire bouillir de l'eau dans le four solaire dans des cannettes et avec le principe de la cafetière italienne l'eau bouillante serait acheminée vers des bocaux isolés dans une boite. Dans les bocaux se trouveraient des capsules hermetiques de paraffine. Le soir quand on aurait besoin d'utiliser le four on ouvrirait la boite isolante qui contient les bocaux pour libérer la chaleur.

<br> Je ne sais pas si ce système fonctionnerait et cela met en jeu des principe scientifiques compliqués. Je voulais en parler à Denis pour qu'il m'éclaire sur le sujet mais il n'était pas là. Finalement Victor m'a demandé si c'était vraiment nécessaire de pouvoir cuire même le soir ; si l'objectif n'est que de pasteuriser alors une utilisation de jour suffit. J'ai donc abandonné l'idée du stockage qui de toute manière semble difficelement résolvable. Il fallait que je trouve un moyen de pasteuriser en maintenant la température à 80°C.

## Semaine 10 - vendredi
<a>
<b>Prototype</b>

<a>Pris de court j'ai eut dû mal à trouver une solution à la pasteurisation à basse température et à la problèmatique des matériaux. J'ai décidé de fabriquer un four solaire à quatre miroirs, inspiré du Global Sun Oven. Pour dimensionner le four solaire je me suis basé sur les capacités d'un four traditionnel. Je voulait que l'on puisse cuisiner des plats familiaux et faire bouillir de l'eau pour toute la famille soit au moins 10L d'eau. Le dimensionnement des pièces du four m'a pris beaucoup de temps. Il m'a fallu une journée pour fabriquer le four. Découper proprement les pièces était plus compliqué que ce que je pensais.
<br>Quand j'ai dû transporter le four je me suis rendu compte qu'il était trop lourd et que son dimensionnement n'était peut-être pas approprié.

<b>Présentation - Feedback</b>

J'ai fait ma présentation et les retours que j'ai eut m'ont beaucoup aidé à mieux cerner les améliorations que je dois apporter. 
<br>Le four tel que je l'ai conçu est beaucoup trop puissant car sa température monte jusqu'à 150°C ce qui est beaucoup trop si on veut pasteuriser les conserves sans cuire les aliments. Il faudrait plutôt un four qui chauffe à 80°C de manière constante. L'idée serait peut-être de retirer certains miroirs et de ventiler la boite pour diminuer la température. 
<br>Les matériaux que j'ai utilisés posent aussi problème car il s'agit de plaques de bois et ce sont des matériaux difficile à se procurer à Cuba. On pourrait imaginer récuperer l'ensemble des matériaux mais ça n'est pas si évident qu'il n'y parait. Une possibilité qu'on m'a suggérée serait de creuser le four dans la terre, ou de le fabriquer en terre. Le problème est que dans ce cas le four ne serait plus mobile et cela restraint encore plus le contexte d'utilisation du four car il faut que les gens aient un habitat avec un jardin ensoleillé ce qui est rare en ville. De plus même à la campagne le soleil peut être en hauteur mais pas à raz du sol. Aussi le four ne pourrait plus être orienté face au soleil en fonction des saisons, mais d'après Denis la perte de puissance ne serait pas si conséquente. 

Il faut donc que je trouve une solution au niveau des matériaux et que je trouve le dispositif adéquat pour réguler la température à 80°C.
</a>

## Semaine 11 - mardi

<p>
Suite aux remarques de la présentation de vendredi je me suis rendu compte que le four solaire de type boite n'était peut-être pas approprié au contexte. En effet le four solaire boite est très efficace, sa température peut monter à 150°C en 1/2h, mais les matériaux qui le composent (bois, isolant, verre...) sont compliqués à se procurer à Cuba. Il faut aussi des outils assez chers pour les cubains que tout le monde ne possède pas comme une scie sauteuse, une perceuse et un coupe-verre. De plus le four solaire n'a pas besoin d'être aussi puissant pour faire des conserves. 

J'ai fait un point sur <b>les méthodes de pasteurisation des conserves</b>, il y a plusieurs possibilités :
<br><b>Dans l'eau</b>
<br>-La pasteurisation à 80°C : elle est intéressante car elle permet de préserver le goût et les nutriments des aliments, mais il est nécessaire d'avoir un indicateur pour savoir si cette température est atteinte. Aussi les fours solaires ne permettent pas une température stable à 80°C, ils se stabilisent à une température supérieur à 100°C.
<br>- La pasteurisation stérilisante à 100°C : cette méthode est la plus commune, par contre elle n'est pas suffisante pour les aliments non acides. Les avis sont assez partagés à ce sujet, certains disent qu'il est possible de faire des conserves d'aliments non acides avec cette méthode à condition de les conserver au frais (température <14°C). Cette méthode à l'avantage de ne nécessiter aucun indicateur de température, dès que le l'eau boue on sait qu'on a atteint 100°C.
<br>- La stérilisation entre 116 et 140°C : elle détruit tous les micro-organismes y compris les spores. Cette température de l'eau ne peut être atteinte qu'avec une autoclave (autocuiseur) qui fait monter la pression pour augmenter la température au delà de 100°C. Elle est préconisée pour les aliments non acides. Les conserves ainsi préparées peuvent être conservées à température ambiante. Cette méthode n'est pas adaptée au contexte de Cuba, car il faut posséder une autoclave onéreuse et chauffer cette ustensile nécessite une puissance conséquente (équivalente à une plaque au gaz).
<br><b>Au four</b>
<br>- Pasteurisation au four à 150°C : La pasteurisation peut se faire au four sans plonger les bocaux dans l'eau. Le four solaire de type boite est idéal pour cette méthode, mais comme expliqué précédement la fabrication d'un tel four pose problème à Cuba.

Source : [Ecoconso - Conserves](https://www.ecoconso.be/fr/content/conservation-comment-steriliser-pasteuriser-les-fruits-et-legumes#_ftn4)

En analysant ces différentes méthodes la plus adaptée est la pasteurisation stérilisante à 100°C. C'est une méthode éprouvée qui ne nécessite ni un autoclave ni un thermomètre. Reste à trouver le type de four le plus adapté à cette température.

J'ai ensuite refait un point sur les différents types de fours et j'ai essayé de trouver les fours avec une fabrication la plus simple possible et qui puissent être adaptée à ma problèmatique de conserves et de manque de matériaux. 
<br>J'ai trouvé un four appelé <b>"pneu cuisinier"</b> qui est fabriqué avec une chambre à air de roue de voiture (camion), une plaque de verre et de polystyrène. Sa fabrication est assez simple mais la récupération de tous ces matériaux n'est pas garantie à Cuba. De plus la documentation sur ce type de four est assez mince, on sait juste que ça utilise le principe d'effet de serre et que ça peut cuire du riz. Aussi ce type de four n'est pas orientable ce qui pose problème quand le soleil est bas.

Sources :
<br>[Pneu cuisinier](http://solarcooking.org/francais/tire_eng-fr.htm)
<br>[Tuto Low Tech Lab - Four solaire chambre à air](https://wiki.lowtechlab.org/wiki/Four_solaire_-_chambre_%C3%A0_air#Commentaires)

Finalement j'ai trouvé le type de four solaire qui pourrait correspondre à ma problématique : <b>les cuiseurs de type panneaux</b>. Ce sont en fait de simple panneaux réflécteurs qui vont renvoyer les rayons du soleil sur un récipient conducteur. Ce récipient métallique et de couleur noire va absorber le chaleur des rayons du soleil ce qui va permettre de cuir des aliments. Pour éviter les déperditions à cause du vent, le récipient est enfermé dans un sac de cuisson transparent ou dans une cloche en verre. La température peut monter jusqu'à 120°C en +/-2h. Ce qui est une température suffisante pour pasteuriser à 100°C.

Sources : 
<br>[Tous les fours solaires](http://solarcooking.org/francais/plans.htm#Boites_-_Fours_solaires)
<br>[Le Cookit](http://solarcooking.org/francais/cookit-fr.htm)

L'avantage de ce type de four c'est que sa fabrication est simple et ne nécessite qu'un matériaux, en général du carton. Il en existe de différentes formes, mais le principe est toujours le même : renvoyer un maximum de rayons vers le récipient. 
<br>Je pense que ce type de four pourrait convenir à ma problématique mais il faudrait que trouve un moyen de le fabriquer avec un autre matériau que du carton, car le carton n'est pas facile à se procurer à Cuba. 

J'ai parlé de ma nouvelle idée aux profs et ils ont l'air plutôt enthousiastes, du coup je vais la développer.

</p>

## Semaine 11 - vendredi

<p>
Suite à l'avis positif des profs j'ai décidé de fabriquer un prototype de cuiseur solaire à panneaux. J'ai analysé les différents modèles existants et le Cookit semble être la référence dans ce domaine. Il se construit grâce à un simple patron en carton et il a fait ses preuves sur le terrains. Cependant le problème est que le miroir avant est difficilement orientable. En hiver le miroir avant doit être quasiment déplié ce qui risque de défaire complétement le pliage et apparement en été son efficacité est remise en cause dans les pays près de l'équateur. Certains modèle ont été conçu pour essayer de resoudre ce problème comme le [DSPC](http://solarcooking.org/francais/DSPC-Cooker-fr.htm) ou le [All Season Solar Cooker](https://www.allseasonsolarcooker.com/). Cependant ces modèles utilisent des pliages assez complexes, le DSPC est trop petit pour l'usage que je souhaite en faire et n'a que 2 positions; l'autre nécessite une quantité importante de carton (polypropylène), des vis et l'orientation des réflécteurs semble difficile à appréhender.

Pour le design de mon cuiseur solaire je me suis inspiré du Cookit tout en essayant d'avoir un panneau orientable en toutes saisons. Je l'ai dimensionné pour faire mes conserves et grâce à fusion j'ai déterminé les angles adéquats pour chaque saison à Cuba. 
<br>Comme il est difficile de se procurer du carton à Cuba j'ai cherché un matériau qui pourrait le remplacer et j'ai pensé au grillage de cage à poule. C'est un matériaux forcément disponible à Cuba puisqu'ils font de l'élevage de volailes. Cependant si à la campagne les habitants en ont certainement dans leur stock, en ville c'est moins sûr, ils devront peut-être en acheter ce qui peut poser problème pour les plus pauvres. L'idée est de se servir du grillage pour accrocher les feuille d'alluminium issue de canettes découpées.
<br> Pour l'orientation du panneau avant j'ai pensé à un système de pieds rabattables qui vont le soulever pour avoir la bonne orientation. L'hiver le panneau est à l'horizontale (sans pieds). Il y a 2 pieds : un pour l'été et un pour les autres saisons. Les pieds sont fabriqués en fil de fer épais et sont retenus par des ficelles. 
<br> En fabriquant le prototype je me suis rendu compte que le fil de fer que j'avais utilisé pour les pieds n'était pas assez épais car il se tordait et les ficelles n'étaient pas assez tendues alors qu'elles assurent la bonne géométrie de l'angle. Aussi je n'avais pas prévu de système assez rigide pour tenir les trois autres panneaux (arrière et latéraux) en garantissant leur bonne orientation et le grillage a tendance à gondoler. Du coup mon assemblage était assez bancale mais il peut être amélioré avec du fil de fer plus épais (2mm min.) et en renforçant les cotés du grillage avec des tiges fines de bambou pour l'aplanir. 
<br>Le cuiseur pourrait même etre fait en bambou. Les tiges de bambou assemblées en grille pourraient servir de support aux miroirs fait à partir de canettes d'alluminium découpées.

J'ai montré mon prototype aux profs et finalement ils m'ont conseillé de faire mon prototype en carton. Même si le carton n'est pas toujours disponible à Cuba l'idée est que le cuiseurs solaire à panneaux peut être fabriqué avec n'importe quel matériau plane, découpable, léger et pliable sur lequel on peut plaquer les canettes découpées pour faire les miroirs. Ainsi il peut être fait en carton, en bois, en grillage ou en bambou, c'est toujours le même principe il faut juste adapté le système de pliage. Avec du bois on mettra des charnières ou du fil, avec du grillage ou du bambou on assemblera avec du fil, avec le carton en pliera ou on assemblera avec du ruban adhésif marron. 
</p>
