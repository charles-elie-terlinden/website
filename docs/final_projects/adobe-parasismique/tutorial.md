# Adobe parasismic

## Tutorial


### How to choose a good clay ? 
#### Example 1 

![1](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/81625914_10220622421215708_8327399918553530368_n.jpg?_nc_cat=110&_nc_ohc=87CWSjhUbxIAQlsOlDPixwcsOHJ9mSuxoEunwF89GnvuLfmwM6algMjRA&_nc_ht=scontent.fbru2-1.fna&oh=c004154210ecd8f4d3be46d5e4a10920&oe=5EA9ED38)

**1.** Take a bucket, shovel, paper, felt

**2.** Dig 20cm below ground level and put the soil in the bucket

**3.** Give each sample a name

![2](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/81808752_10220622422615743_4250718833519624192_o.jpg?_nc_cat=111&_nc_ohc=0SihxET-pmAAQnQU0GXzN87cWom9f6lETtbfo5LmBuiwc1O1QGa3FKhfw&_nc_ht=scontent.fbru2-1.fna&oh=9fa76e898c6854e8d9f1439e632fe437&oe=5E9ABB84)

**1.** Take three identical jars and make a line at two thirds

**2.** Fill each jar with a sample of soil, respecting the line. Each sample must be sieved to 0.5cm

**3.** Fill each jar with water

**4.** Shake vigorously for 2 minutes

**5.** Let sit for 24 hours

**6.** Result after 24h


![3](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/82070840_10220622422775747_2626183037611671552_o.jpg?_nc_cat=102&_nc_ohc=_x4ogpfhs9kAQmXkzOyna-0jUYVPMPXUrLr3EuONVrnVmjn2z1dTzmbfw&_nc_ht=scontent.fbru2-1.fna&oh=8eb4352c5936cc892f3fdf1d8eabe81f&oe=5EA1E6A4)


Observe each layer of sediments of different natures (sand, silt, clay) and note their thicknesses

![bis3](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/82360166_10220622930748446_4011033626606043136_n.jpg?_nc_cat=111&_nc_ohc=pVi6cEdfgkIAQl8MvKS5mXpI9VkYSW6Z56aIlEkxYNFJ4qS6mOUAWMxzQ&_nc_ht=scontent.fbru2-1.fna&oh=c1598f0d1b70c4f54018e6c558883337&oe=5E9A290E)

Calculate the proportions of each layer and define the nature of the soil. It must be clay.

#### Example 2

![4](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/81886394_10220622561859224_4024008443634384896_o.jpg?_nc_cat=104&_nc_ohc=fX248mwogzEAQn5LqxXTz_Nya5ndYyaE2IkrMRVYkqmtN_0G8WrkGYWDQ&_nc_ht=scontent.fbru2-1.fna&oh=90cef995d1318b6446bc9d07602fd54b&oe=5EACA8A5)

**1.**  Take a trowel, and a bucket

**2.** Moisten the test soil and wait for 12 hours

**3.** Shape a ball the size of a grapefruit


![5](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/81789874_10220622560699195_6727117867390599168_o.jpg?_nc_cat=100&_nc_ohc=Gj72hOqnuwgAQm9DGoMUAIvTtrRgmADJWKBDNBYV73biTZuRM7gf_tYCQ&_nc_ht=scontent.fbru2-1.fna&oh=a58e9e796272d5214edb68f6aff407e1&oe=5E947543)

**1.** Shape a sausage without just rolling it with finger pressure

**2.** Obtain a sausage 3-4cm in diameter

**3.** Lift it carefully and slide it so that it breaks naturally

**4.** Measure the length of the sausage and if it between 5 and 14cm , the soil is good for adobe


### How to make the mold ? 

![6](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/82122545_10220622807345361_8908003497526427648_o.jpg?_nc_cat=102&_nc_ohc=r6bhDuEY-KQAQnHZeskMYjGAu3440bzrKJJq1XC2F9mwFkBtqTSiDNGVw&_nc_ht=scontent.fbru2-1.fna&oh=8b9e494f4c7a9884c0f1b1255c1586fd&oe=5EABDB19)

You can program a CNC to cut. It is also possible to create it from low-tech means. 

An STL file. corresponding to the model of the mold is available through this link :
[moule avec poignées](/docs/final_projects/adobe-parasismique/Fichier moule stl/Moule_poignees_v3.stl)
[moule simple](/docs/final_projects/adobe-parasismique/Fichier moule stl/Moule_30cm_v5.stl)

![7](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-0/p206x206/82297092_10220622806705345_6887554519601774592_o.jpg?_nc_cat=111&_nc_ohc=4hs8GlvZ68gAQn68rIZn_M_7BAVLR8UIaZbK0p7ed5D8wvCB4WYdZJoSw&_nc_ht=scontent.fbru2-1.fna&_nc_tp=1&oh=a5c1848010251272a5707dfde67e2951&oe=5EB3161D)

You get these two plates cut to size.

![8](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/s960x960/81481906_10220622753184007_5377791004441051136_o.jpg?_nc_cat=105&_nc_ohc=aaL81FlZC7wAQnDURAlkomGHESTzifNBfoXc7wpFNPjO7JY9LHtSQ0e_Q&_nc_ht=scontent.fbru2-1.fna&_nc_tp=1&oh=de244d6e2b20e43cc60b8113ea603d41&oe=5EA98BA8)

You must assemble these boards in this order using glue and / or nails. 

![8](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-0/p206x206/81870128_10220623126793347_5055326680923504640_o.jpg?_nc_cat=111&_nc_ohc=IjGwTHmJFbwAQnl3rBkIuliywyEhtOUAxIyHdKi7OVlkKOi2tiOtsfb4Q&_nc_ht=scontent.fbru2-1.fna&_nc_tp=1&oh=9fe44afb91734c9df4adac5a23f3af05&oe=5E94DB5E)

The mold is ready to use ! It's your turn !

### How construct a wall ? 

![9](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/s960x960/81892090_10220624486747345_7806062490218921984_o.jpg?_nc_cat=102&_nc_ohc=z4gQI35DFWAAQkuRtIvWsD62IxEN4WQp5NklfwoVPQDLPEh9TLPf4fVsg&_nc_ht=scontent.fbru2-1.fna&_nc_tp=1&oh=ebdde368cbc288f6272ca260c7ca3b36&oe=5E9BF125)

Just assemble the adobe bricks using the equipment described above.

![10](https://scontent.fbru2-1.fna.fbcdn.net/v/t1.0-9/s960x960/81626202_10220624388984901_5228003753705078784_o.jpg?_nc_cat=108&_nc_ohc=mx3rPbNGT2EAQlwdHz96UIurjUAREn6X_Ybb8LKpuQcGsHipVtKxxglcQ&_nc_ht=scontent.fbru2-1.fna&_nc_tp=1&oh=06a8940df26458c4b9328267dac6d636&oe=5E958AE2)


**1.** To make the mortar, add water so that the soil is very plastic and spreads easily with a trowel. Do not flatten the surface so that the brick adhere to the maximum during assembly. 

**2.** Mount the bricks, seat by seat, insisting on the gaps.

**3.** Finally, the joints of the facing face can be made after 24 hours of drying. The finish is up to everyone's choice (smooth, rugged, ...)

